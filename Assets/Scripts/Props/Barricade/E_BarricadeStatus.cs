﻿public enum E_BarricadeStatus
{
    FullHP,
    HalfHP,
    LowHP,
    Dead,
}