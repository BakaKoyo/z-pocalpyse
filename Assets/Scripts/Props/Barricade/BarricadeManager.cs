﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class BarricadeManager : MonoBehaviour, I_Attackable
{

    [Header("Barricade Effects")]
    [SerializeField] private List<GameObject> _barricadeEffects;
    [SerializeField] [Range(0, 5)] private int _barricadeFullHP_Effect = 0;
    [SerializeField] [Range(0, 5)] private int _barricadeHalfHP_Effect = 1;
    [SerializeField] [Range(0, 5)] private int _barricadeLowHP_Effect = 2;
    [SerializeField] [Range(0, 5)] private int _barricadeDeadHP_Effect = 3;

    [Header("Barricade HP Settings")]
    [SerializeField] [Range(0f, 100f)] private float _barricadeFullHP = 100f;
    [SerializeField] [Range(0f, 100f)] private float _barricadeHalfHP = 50f;
    [SerializeField] [Range(0f, 100f)] private float _barricadeLowHP = 25f;
    [SerializeField] [Range(0f, 100f)] private float _barricadeDead = 0f;

    [SerializeField] private BoxCollider _barrier;

    private List<IDisposable> _barricadeListeners = new List<IDisposable>();

    private BehaviorSubject<E_BarricadeStatus> _BarricadeStatus = new BehaviorSubject<E_BarricadeStatus>(E_BarricadeStatus.FullHP);
    private ReactiveProperty<float> _barricadeCurrentHP = new ReactiveProperty<float>(100f);
    private ReactiveProperty<int> _barricadeCurrentElement = new ReactiveProperty<int>(0);

    private ReactiveProperty<bool> _isPropDead = new ReactiveProperty<bool>(false);
    private IReactiveProperty<bool> IsPropDead => _isPropDead;


    private void Awake()
    {
        ChangeBarricadeVisibility();
    }

    private void ChangeBarricadeVisibility()
    {
        for (int i = 0; i < _barricadeEffects.Count; i++)
        {

            if (_barricadeEffects[i].activeSelf == true &&
                _barricadeEffects[i] != _barricadeEffects[_barricadeCurrentElement.Value])
            {
                _barricadeEffects[i].SetActive(false);
            }
            else if (_barricadeEffects[i].activeSelf == false &&
                    _barricadeEffects[i] != _barricadeEffects[_barricadeCurrentElement.Value])
            {
                _barricadeEffects[i].SetActive(false);
            }
            else
            {
                _barricadeEffects[i].SetActive(true);
            }

        }
    }



    private void Start()
    {

        #region [ Value Observables ]

        _barricadeCurrentHP.AsObservable()
                           .Where(HP => HP <= _barricadeFullHP &&
                                        HP > _barricadeHalfHP)
                           .Subscribe(HP =>
                           {
                               _BarricadeStatus.OnNext(E_BarricadeStatus.FullHP);
                           })
                           .AddTo(this);

        _barricadeCurrentHP.AsObservable()
                          .Where(HP => HP <= _barricadeHalfHP &&
                                       HP > _barricadeLowHP)
                          .Subscribe(HP =>
                          {
                              _BarricadeStatus.OnNext(E_BarricadeStatus.HalfHP);
                          })
                          .AddTo(this);

        _barricadeCurrentHP.AsObservable()
                          .Where(HP => HP <= _barricadeLowHP &&
                                       HP > _barricadeDead)
                          .Subscribe(HP =>
                          {
                              _BarricadeStatus.OnNext(E_BarricadeStatus.LowHP);
                          })
                          .AddTo(this);

        _barricadeCurrentHP.AsObservable()
                          .Where(HP => HP <= _barricadeDead)
                          .Subscribe(HP =>
                          {
                              _BarricadeStatus.OnNext(E_BarricadeStatus.Dead);
                          })
                          .AddTo(this);

        #endregion


        #region [ States ]

        _BarricadeStatus.Subscribe(state => Utilities.CleanUpListenersInStream(_barricadeListeners));

        _BarricadeStatus.Where(state => state == E_BarricadeStatus.FullHP)
                        .Subscribe(state =>
                        {
                            OnFullHP();
                        })
                        .AddTo(this);

        _BarricadeStatus.Where(state => state == E_BarricadeStatus.HalfHP)
                        .Subscribe(state =>
                        {
                            OnHalfHP();
                        })
                        .AddTo(this);

        _BarricadeStatus.Where(state => state == E_BarricadeStatus.LowHP)
                        .Subscribe(state =>
                        {
                            OnLowHP();
                        })
                        .AddTo(this);

        _BarricadeStatus.Where(state => state == E_BarricadeStatus.Dead)
                        .Subscribe(state =>
                        {
                            OnDeadHP();
                        })
                        .AddTo(this);

        #endregion


    }


    #region [ State Machine ]

    private void OnFullHP()
    {
        _barricadeListeners.Add(Observable.EveryUpdate()
                                          .Subscribe(_ =>
                                          {
                                              _barricadeCurrentElement.Value = _barricadeFullHP_Effect;
                                              ChangeBarricadeVisibility();
                                          }));
    }

    private void OnHalfHP()
    {
        _barricadeListeners.Add(Observable.EveryUpdate()
                                          .Subscribe(_ =>
                                          {
                                              _barricadeCurrentElement.Value = _barricadeHalfHP_Effect;
                                              ChangeBarricadeVisibility();

                                          }));
    }

    private void OnLowHP()
    {
        _barricadeListeners.Add(Observable.EveryUpdate()
                                          .Subscribe(_ =>
                                          {
                                              _barricadeCurrentElement.Value = _barricadeLowHP_Effect;
                                              ChangeBarricadeVisibility();
                                          }));
    }


    private void OnDeadHP()
    {
        _barricadeListeners.Add(Observable.EveryUpdate()
                                          .Subscribe(_ =>
                                          {
                                              _barricadeCurrentElement.Value = _barricadeDeadHP_Effect;
                                              ChangeBarricadeVisibility();
                                              _barrier.enabled = false;
                                              _isPropDead.Value = true;
                                          }));
    }

    #endregion


    #region [ Interfaces ]

    public void OnHit(float value)
    {
        _barricadeCurrentHP.Value -= value;
        EventManager.instance.SendEvent<float>(EventIDs._UPDATE_BARRIER_HP, _barricadeCurrentHP.Value);
    }

    public IReactiveProperty<bool> UnitDead()
    {
        return IsPropDead;
    }

    #endregion


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<I_Attackable>() != null)
        {
            other.gameObject.GetComponent<ZombieController>()._isBarricadeDestroyed.Value = true;
        }
    }


}
