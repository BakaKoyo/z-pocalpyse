﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
public class PlayerController : Player, I_Attackable
{

    [Header("Keybinds")]
    [SerializeField] protected KeyCode _key_MoveUp = KeyCode.W;
    [SerializeField] protected KeyCode _key_MoveDown = KeyCode.S;
    [SerializeField] protected KeyCode _key_ShootWeapon = KeyCode.F;

    protected void Awake()
    {
        /* Components */
        _unitRB = GetTypeOf(_unitRB);


        /* Default Settings */
        _unitHP = Utilities.DefaultValueOf(_unitHP, 1f);
        _unitMoveSpeed = Utilities.DefaultValueOf(_unitMoveSpeed, 7f);


        /* Setups */
        _isUnitDead.Value = false;
    }


    protected void Start()
    {

        EventManager.instance.AddListener(EventIDs._ON_MOVE_UP, MovePlayerUP);
        EventManager.instance.AddListener(EventIDs._ON_MOVE_DOWN, MovePlayerDOWN);

        #region [ Value Observables ]

        _isUnitDead.AsObservable()
                   .Where(isDead => isDead == false)
                   .Subscribe(isDead =>
                   {
                       _PlayerStatus.OnNext(E_PlayerStatus.Alive);
                   })
                   .AddTo(this);

        _isUnitDead.AsObservable()
                   .Where(isDead => isDead == true)
                   .Subscribe(isDead =>
                   {
                       _PlayerStatus.OnNext(E_PlayerStatus.Dead);
                   })
                   .AddTo(this);

        #endregion


        #region [ States ]

        _PlayerStatus.Subscribe(_ => Utilities.CleanUpListenersInStream(_listener_UnitStatusStreams));

        _PlayerStatus.Where(state => state == E_PlayerStatus.Alive)
                    .Subscribe(state =>
                    {
                        AliveState();
                    })
                    .AddTo(this);


        _PlayerStatus.Where(state => state == E_PlayerStatus.Dead)
                     .Subscribe(state =>
                     {
                     })
                     .AddTo(this);

        #endregion

    }

    protected void OnDisable()
    {
        EventManager.instance.RemoveListener(EventIDs._ON_MOVE_UP, MovePlayerUP);
        EventManager.instance.RemoveListener(EventIDs._ON_MOVE_DOWN, MovePlayerDOWN);
    }

    #region [ State Machine ]

    protected void AliveState()
    {

        _listener_UnitStatusStreams
            .Add(Observable.EveryUpdate()
            .Subscribe(_ =>
            {
                _isUnitDead.Value = CheckIfUnitIsDead(_unitHP);


                #region [ Movement ]

                switch (_PlayerMove)
                {

                    case E_PlayerMove.Idle:

                        if(CheckforMoveInputs() == true)
                        {
                            _PlayerMove = E_PlayerMove.Moving;
                        }

                        break;

                    case E_PlayerMove.Moving:

                        if(CheckforMoveInputs() == false)
                        {
                            _PlayerMove = E_PlayerMove.Idle;
                        }
                        else
                        {
                            MovePlayer();
                        }

                        break;
                }

                #endregion


                #region [ Shooting ]

                switch (_PlayerShoot)
                {

                    case E_PlayerShoot.Idle:

                        if(CheckforShootInputs() == true)
                        {
                            _PlayerShoot = E_PlayerShoot.Shooting;
                        }

                        break;

                    case E_PlayerShoot.Shooting:

                        if (CheckforShootInputs() == false)
                        {
                            _PlayerShoot = E_PlayerShoot.Idle;
                        }
                        else
                        {
                            _weapons[_currentWeapon].ShootWeapon();
                        }

                        break;
                    
                }

                #endregion

            }));

    }

    #endregion


    #region [ Helpers ]

    protected bool CheckforMoveInputs()
    {
        if (Input.GetKey(_key_MoveUp) ||
            Input.GetKey(_key_MoveDown))
        {
            return true;
        }
        else return false;
    }

    protected bool CheckforShootInputs()
    {
        if (Input.GetKey(_key_ShootWeapon))
        {
            return true;
        }
        else return false;
    }

    #endregion


    protected void MovePlayer()
    {
        if (Input.GetKey(_key_MoveUp))
        {
            _unitRB.MovePosition(transform.position +
                                 transform.right *
                                 _unitMoveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(_key_MoveDown))
        {
            _unitRB.MovePosition(transform.position +
                                 transform.right *
                                 -_unitMoveSpeed * Time.deltaTime);
        }
    }
    protected void MovePlayerUP()
    {
        _PlayerMove = E_PlayerMove.Moving;
        _unitRB.MovePosition(transform.position +
                                 transform.right *
                                 _unitMoveSpeed * Time.deltaTime);
    }
    protected void MovePlayerDOWN()
    {
        _PlayerMove = E_PlayerMove.Moving;
        _unitRB.MovePosition(transform.position +
                                   transform.right *
                                   -_unitMoveSpeed * Time.deltaTime);
    }



    #region [ Interfaces ]

    public void OnHit(float value)
    {
        _unitHP -= value;
    }

    public IReactiveProperty<bool> UnitDead()
    {
        return IsUnitDead;
    }

    #endregion

}
