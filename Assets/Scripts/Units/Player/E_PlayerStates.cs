﻿public enum E_PlayerStatus
{
    Alive,
    Dead,
}

public enum E_PlayerMove
{
    Idle,
    Moving,
}

public enum E_PlayerShoot
{
    Idle,
    Shooting,
}

