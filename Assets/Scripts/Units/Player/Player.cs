﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


public class Player : BaseUnit
{
    [Header("Player States")]
    [SerializeField] public E_PlayerMove _PlayerMove = E_PlayerMove.Idle;
    [SerializeField] protected E_PlayerShoot _PlayerShoot = E_PlayerShoot.Idle;


    [Header("Weapons")]
    [SerializeField] [Range(0, 10)] protected int _currentWeapon;
    [SerializeField] protected List<Weapon> _weapons = new List<Weapon>(); // If I wanted to add more weapons 

       
    /* States */
    public BehaviorSubject<E_PlayerStatus> _PlayerStatus = new BehaviorSubject<E_PlayerStatus>(E_PlayerStatus.Alive);


}
