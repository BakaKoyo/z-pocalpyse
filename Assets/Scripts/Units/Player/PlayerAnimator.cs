﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


[RequireComponent(typeof(Animator))]
public class PlayerAnimator : Player
{

    [Header("Animations [ Action ]")]
    [SerializeField] [Range(0f, 1f)] protected float _actionShoot = 0f;
    [SerializeField] [Range(0f, 1f)] protected float _actionDead = 1f;

    [Header("Animations [ Movement ]")]
    [SerializeField] [Range(0f, 1f)] protected float _animIdle = 0f;
    [SerializeField] [Range(0f, 1f)] protected float _animMoving = 0.5f;
    [SerializeField] [Range(0f, 1f)] protected float _animDead = 1f;

    [Header("Collider")]
    [SerializeField] protected CapsuleCollider _collider;


    protected string _movementParam = EventIDs._ANIMATION_MOVEMENT_PARAM;
    protected string _actionParam = EventIDs._ANIMATION_ACTION_PARAM;


    protected PlayerController _playerControllerRef;

    protected void Awake()
    {
        _unitAnimator = GetTypeOf(_unitAnimator);
        _playerControllerRef = FindObjectOfType<PlayerController>();
    }

    protected void Start()
    {

        EventManager.instance.AddListener(EventIDs._ON_MOVE_UP, MovePlayerUP);
        EventManager.instance.AddListener(EventIDs._ON_MOVE_DOWN, MovePlayerDOWN);

        _playerControllerRef._PlayerStatus.Subscribe(_ => Utilities.CleanUpListenersInStream(_listener_UnitStatusStreams));

        _playerControllerRef._PlayerStatus.Where(state => state == E_PlayerStatus.Alive)
                                          .Subscribe(state =>
                                          {
                                              AliveState();
                                          })
                                          .AddTo(this);

        _playerControllerRef._PlayerStatus.Where(state => state == E_PlayerStatus.Dead)
                                         .Subscribe(state =>
                                         {
                                             DeadState();
                                         })
                                         .AddTo(this);

    }

    protected void AliveState()
    {
        _listener_UnitStatusStreams.Add(Observable.EveryUpdate()
                                                  .Subscribe(_ =>
                                                  {
                                                      switch (_playerControllerRef._PlayerMove)
                                                      {
                                                          case E_PlayerMove.Idle:

                                                              PlayAnimation(_unitAnimator, _movementParam, _animIdle);

                                                              break;

                                                          case E_PlayerMove.Moving:

                                                              PlayAnimation(_unitAnimator, _movementParam, _animMoving);

                                                              break;
                                                      }
                                                  }));
    }

    protected void DeadState()
    {
        _listener_UnitStatusStreams.Add(Observable.EveryUpdate()
                                                  .Subscribe(_ =>
                                                  {
                                                      PlayAnimation(_unitAnimator, _movementParam, _animDead);
                                                      PlayAnimation(_unitAnimator, _actionParam, _actionDead);
                                                      _collider.enabled = false;
                                                  }));
    }


    protected void MovePlayerUP()
    {
        PlayAnimation(_unitAnimator, _movementParam, _animMoving);
    }
    protected void MovePlayerDOWN()
    {
        PlayAnimation(_unitAnimator, _movementParam, _animMoving);
    }

}
