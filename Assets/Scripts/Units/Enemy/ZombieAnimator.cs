﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


[RequireComponent(typeof(Animator))]
public class ZombieAnimator : Zombie
{

    [Header("Animations [ Action ]")]
    [SerializeField] [Range(0f, 1f)] protected float _actionIdle = 0f;
    [SerializeField] [Range(0f, 1f)] protected float _actionAttack = 0.5f;
    [SerializeField] [Range(0f, 1f)] protected float _actionDead = 1f;

    [Header("Animations [ Movement ]")]
    [SerializeField] [Range(0f, 1f)] protected float _animIdle = 0f;
    [SerializeField] [Range(0f, 1f)] protected float _animMoving = 0.3f;
    [SerializeField] [Range(0f, 1f)] protected float _animRuning = 0.6f;
    [SerializeField] [Range(0f, 1f)] protected float _animDead = 1f;

    [Header("Collider")]
    [SerializeField] protected CapsuleCollider _collider;

    [SerializeField] protected ZombieController _zombieControllerRef;

    protected string _movementParam = EventIDs._ANIMATION_MOVEMENT_PARAM;
    protected string _actionParam = EventIDs._ANIMATION_ACTION_PARAM;


    protected void Awake()
    {
        _unitAnimator = GetTypeOf(_unitAnimator);
    }

    protected void Start()
    {
        _zombieControllerRef._ZombieStatus.Subscribe(_ => Utilities.CleanUpListenersInStream(_listener_UnitStatusStreams));

        _zombieControllerRef._ZombieStatus.Where(state => state == E_ZombieStatus.Alive)
                                  .Subscribe(state =>
                                  {
                                      AliveState();
                                  })
                                  .AddTo(this);

        _zombieControllerRef._ZombieStatus.Where(state => state == E_ZombieStatus.Dead)
                                         .Subscribe(state =>
                                         {
                                             DeadState();
                                         })
                                         .AddTo(this);
    }


    protected void AliveState()
    {
        _listener_UnitStatusStreams.Add(Observable.EveryUpdate()
                                                  .Subscribe(_ =>
                                                  {
                                                      switch (_zombieControllerRef._ZombieMove)
                                                      {
                                                          case E_ZombieMove.Idle:

                                                              PlayAnimation(_unitAnimator, _movementParam, _animIdle);

                                                              break;

                                                          case E_ZombieMove.Moving:

                                                              PlayAnimation(_unitAnimator, _movementParam, _animRuning);

                                                              break;
                                                      }

                                                      switch (_zombieControllerRef._ZombieAction)
                                                      {
                                                          case E_ZombieAction.Idle:

                                                              PlayAnimation(_unitAnimator, _actionParam, _actionIdle);

                                                              break;

                                                          case E_ZombieAction.Attacking:

                                                              PlayAnimation(_unitAnimator, _actionParam, _actionAttack);

                                                              break;
                                                      }
                                                  }));
    }

    protected void DeadState()
    {
        _listener_UnitStatusStreams.Add(Observable.EveryUpdate()
                                                  .Subscribe(_ =>
                                                  {
                                                      PlayAnimation(_unitAnimator, _movementParam, _animDead);
                                                      PlayAnimation(_unitAnimator, _actionParam, _actionDead);
                                                      Destroy(this);
                                                      _collider.enabled = false;
                                                  }));
    }

}
