﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UniRx;

public class Zombie : BaseUnit
{

    [Header("Zombie Type")]
    [SerializeField] protected E_ZombieType _ZombieType;
    [SerializeField] [Range(0f, 90f)] protected float _minChanceOfType = 0f;
    [SerializeField] [Range(0f, 90f)] protected float _maxChanceOfType = 90f;
    [Header("Normal Zombies Properties")]
    [SerializeField] [Range(0f, 500f)] protected float _normal_MaxHP;
    [SerializeField] [Range(0f, 500f)] protected float _normal_MinHP;
    [SerializeField] [Range(0f, 30f)] protected float _normal_MoveSpeed;
    [SerializeField] [Range(0f, 30f)] protected float _normal_DMG;
    [Header("Runner Zombies Properties")]
    [SerializeField] [Range(0f, 500f)] protected float _runner_MaxHP;
    [SerializeField] [Range(0f, 500f)] protected float _runner_MinHP;
    [SerializeField] [Range(0f, 30f)] protected float _runner_MoveSpeed;
    [SerializeField] [Range(0f, 30f)] protected float _runner_DMG;
    [Header("Tanky Zombies Properties")]
    [SerializeField] [Range(0f, 500f)] protected float _tanky_MaxHP;
    [SerializeField] [Range(0f, 500f)] protected float _tanky_MinHP;
    [SerializeField] [Range(0f, 30f)] protected float _tanky_MoveSpeed;
    [SerializeField] [Range(0f, 30f)] protected float _tanky_DMG;

    [Header("Event Settings")]
    [SerializeField] [Range(0f, 2f)] protected float _moveSpeedWhenBarrierDestroyed;
    [SerializeField] [Range(0f, 2f)] protected float _timeTillDeath;

    [Header("Attack Settings")]
    [SerializeField] [Range(0f, 2f)] protected float _attackSpeed;

    [Header("Attack Effects")]
    [SerializeField] [Range(0f, 1f)] protected float _timeAnimationAttack;
    [SerializeField] protected GameObject _barricadeDmgEffect;
    [SerializeField] [Range(0f, 1f)] protected float _barricadeEffectDurration;

    [Header("Zombie States")]
    [SerializeField] public E_ZombieMove _ZombieMove = E_ZombieMove.Idle;
    [SerializeField] public E_ZombieAction _ZombieAction = E_ZombieAction.Idle;

    [Header("AI Target")]
    [SerializeField] protected PlayerController _target;

    [Header("AI Path")]
    [SerializeField] protected PathMaker _unitPath;
    [SerializeField] protected List<PathMaker> _unitAvailablePaths;
    [SerializeField] [Range(0f, 1f)] protected float _stoppingDistanceFromDestination = 0.75f;
    [SerializeField] protected int _currentPathElement = 0;

    [Header("Raycast Settings")]
    [SerializeField] protected Transform _attackSpot;
    [SerializeField] [Range(0f, 20f)] protected float _attackRange;

    [Header("Current Unit Values")]
    [SerializeField] protected float HP;
    [SerializeField] protected float MoveSpeed;
    [SerializeField] protected float Damage;
    


    /* States */
    public BehaviorSubject<E_ZombieStatus> _ZombieStatus = new BehaviorSubject<E_ZombieStatus>(E_ZombieStatus.Alive);

    /* AI stuff */
    protected float _unitDmg;
    protected float _unitMovespeedTowardsPlayer = 2f;

    protected ReactiveProperty<bool> _inRangeOfAttackable = new ReactiveProperty<bool>(false);
    protected ReactiveProperty<bool> _canAttack = new ReactiveProperty<bool>(true);
    public ReactiveProperty<bool> _isBarricadeDestroyed = new ReactiveProperty<bool>(false);

    protected NavMeshAgent _zombieAgent;
    protected int _unitMaxAvailablePaths;
    protected int _unitMinAvailablePaths = 0;

    protected GameObject _impactEffect;

    #region [ Helpers ]

    protected E_ZombieType GenerateZombieType()
    {
        float rndNum = Random.Range(_minChanceOfType, _maxChanceOfType);

        /* Normal Zombie Types */
        if(rndNum <= 30f)
        {
            return E_ZombieType.Normal;
        }
        /* Runner Zombie Types */
        else if(rndNum <= 60f && rndNum > 30f)
        {
            return E_ZombieType.Runner;
        }
        /* Tank Zombie Types */
        else
        {
            return E_ZombieType.Tanky;
        }
    }

    protected void SetZombieType()
    {
        float rndHPNum;

        switch (_ZombieType)
        {

            case E_ZombieType.Normal:

                rndHPNum = Random.Range(_normal_MinHP, _normal_MaxHP);
                _unitHP = rndHPNum;
                _unitMoveSpeed = _normal_MoveSpeed;
                _unitDmg = _normal_DMG;

                _zombieAgent.speed = _unitMoveSpeed;

                break;

            case E_ZombieType.Runner:

                rndHPNum = Random.Range(_runner_MinHP, _runner_MaxHP);
                _unitHP = rndHPNum;
                _unitMoveSpeed = _runner_MoveSpeed;
                _unitDmg = _runner_DMG;

                _zombieAgent.speed = _unitMoveSpeed;

                break;

            case E_ZombieType.Tanky:

                rndHPNum = Random.Range(_tanky_MinHP, _tanky_MaxHP);
                _unitHP = rndHPNum;
                _unitMoveSpeed = _tanky_MoveSpeed;
                _unitDmg = _tanky_DMG;

                _zombieAgent.speed = _unitMoveSpeed;

                break;
        }
    }

    /* Generate a random path for this unit */
    protected void SetUnitPath()
    {
        int rndPath = Random.Range(_unitMinAvailablePaths, _unitMaxAvailablePaths);

        _unitPath = _unitAvailablePaths[rndPath];
    }

    protected void CheckforAttackable()
    {
        RaycastHit rayHit;

        if(Physics.Raycast(_attackSpot.position, _attackSpot.forward,
                           out rayHit, _attackRange))
        {
            I_Attackable unitHit = rayHit.transform.GetComponent<I_Attackable>();

            /*
            If it hits a component that 
            is Attackable return true 
            */
            if (unitHit != null &&
                rayHit.transform.tag != EventIDs._TAG_ENEMY &&
                unitHit.UnitDead().Value == false)
            {
                _inRangeOfAttackable.Value = true;
            }
            else
            {
                _inRangeOfAttackable.Value = false;
            }
        }
        else
        {
            _inRangeOfAttackable.Value = false;
        }

    }


    #endregion


    #region [ Functionality ]

    protected void MoveUnit()
    {
        if (Vector3.Distance(_unitPath._wayPoints[_currentPathElement],
                             transform.position) <= _stoppingDistanceFromDestination)
        {
            _currentPathElement++;
            if (_currentPathElement >= _unitPath._wayPoints.Count)
            {
                _currentPathElement = 0;
            }
        }
        else
        {
            _zombieAgent.SetDestination(_unitPath._wayPoints[_currentPathElement]);
        }
    }

    protected void AttackTarget()
    {
        if(_canAttack.Value == true)
        {
            _canAttack.Value = false;
            StartCoroutine(OnAttack());
        }
    }

    protected IEnumerator OnAttack()
    {
        OnAttackStart();
        yield return new WaitForSeconds(_attackSpeed);
        OnAttackEnd();
    }
    protected void OnAttackStart()
    {
        RaycastHit rayHit;

        if (Physics.Raycast(_attackSpot.position, _attackSpot.forward,
                          out rayHit, _attackRange))
        {
            I_Attackable unitHit = rayHit.transform.GetComponent<I_Attackable>();

            /*
            If it hits a component that 
            is Attackable return true 
            */
            if (unitHit != null && 
                unitHit.UnitDead().Value == false)
            {
                unitHit.OnHit(_unitDmg);
            }

            if(rayHit.transform.tag == EventIDs._TAG_BARRIER)
            {
                StartCoroutine(WoodEffect(_barricadeDmgEffect, rayHit));
            }
        }

    }
    protected void OnAttackEnd()
    {
        _canAttack.Value = true;
    }


    private IEnumerator WoodEffect(GameObject pEffect, RaycastHit hit)
    {
        yield return new WaitForSeconds(_timeAnimationAttack);
        _impactEffect = Instantiate(pEffect, hit.point, Quaternion.LookRotation(hit.normal));
        yield return new WaitForSeconds(_barricadeEffectDurration);
        Destroy(_impactEffect);
    }

    #endregion

}
