﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UniRx;


[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider),
                  typeof(NavMeshAgent))]
public class ZombieController : Zombie, I_Attackable
{

    protected void Awake()
    {
        _unitRB = GetTypeOf(_unitRB);
        _zombieAgent = GetTypeOf(_zombieAgent);

        _unitMaxAvailablePaths = _unitAvailablePaths.Count;
        _isUnitDead.Value = false;

        /* Make sure all inspector value are set */
        _normal_MaxHP = Utilities.DefaultValueOf(_normal_MaxHP, 100f);
        _normal_MinHP = Utilities.DefaultValueOf(_normal_MinHP, 50f);
        _normal_MoveSpeed = Utilities.DefaultValueOf(_normal_MoveSpeed, 8f);
        _normal_DMG = Utilities.DefaultValueOf(_normal_DMG, 1f);

        _runner_MaxHP = Utilities.DefaultValueOf(_runner_MaxHP, 150f);
        _runner_MinHP = Utilities.DefaultValueOf(_runner_MinHP, 60f);
        _runner_MoveSpeed = Utilities.DefaultValueOf(_runner_MoveSpeed, 15f);
        _runner_DMG = Utilities.DefaultValueOf(_runner_DMG, 2f);

        _tanky_MaxHP = Utilities.DefaultValueOf(_tanky_MaxHP, 300f);
        _tanky_MinHP = Utilities.DefaultValueOf(_tanky_MinHP, 100f);
        _tanky_MoveSpeed = Utilities.DefaultValueOf(_tanky_MoveSpeed, 4f);
        _tanky_DMG = Utilities.DefaultValueOf(_tanky_DMG, 1f);

        _ZombieType = GenerateZombieType();
        SetZombieType();

        SetUnitPath();

        _target = FindObjectOfType<PlayerController>();

        /* Temp for Balancing values later */
        HP = _unitHP;
        MoveSpeed = _unitMoveSpeed;
        Damage = _unitDmg;
    }


    protected void Start()
    {

        #region [ Value Observables ]

        _isUnitDead.AsObservable()
                   .Where(isDead => isDead == false)
                   .Subscribe(isDead =>
                   {
                       _ZombieStatus.OnNext(E_ZombieStatus.Alive);
                   })
                   .AddTo(this);

        _isUnitDead.AsObservable()
                   .Where(isDead => isDead == true)
                   .Subscribe(isDead =>
                   {
                       _ZombieStatus.OnNext(E_ZombieStatus.Dead);
                   })
                   .AddTo(this);

        #endregion


        #region [ States ]

        _ZombieStatus.Subscribe(_ => Utilities.CleanUpListenersInStream(_listener_UnitStatusStreams));

        _ZombieStatus.Where(state => state == E_ZombieStatus.Alive)
                    .Subscribe(state =>
                    {
                        AliveState();

                    })
                    .AddTo(this);


        _ZombieStatus.Where(state => state == E_ZombieStatus.Dead)
                     .Subscribe(state =>
                     {
                         StartCoroutine(DeadState());
                     })
                     .AddTo(this);

        #endregion

    }


    #region [ Events ]

    protected void TargetPlayer()
    {
        _zombieAgent.SetDestination(_target.transform.position);
        _zombieAgent.speed = _moveSpeedWhenBarrierDestroyed;
    }

    #endregion


    #region [ State Machine ]


    protected void AliveState()
    {
        _listener_UnitStatusStreams
            .Add(Observable.EveryUpdate()
            .Subscribe(_ =>
            {
                _isUnitDead.Value = CheckIfUnitIsDead(_unitHP);

                CheckforAttackable();

                Debug.DrawRay(_attackSpot.position, _attackSpot.transform.forward * _attackRange, Color.yellow);

                #region [ Movement ]

                switch (_ZombieMove)
                {

                    case E_ZombieMove.Idle:

                        if(_inRangeOfAttackable.Value == false)
                        {
                            _ZombieMove = E_ZombieMove.Moving;
                        }
                        else if(_isBarricadeDestroyed.Value == true)
                        {
                            TargetPlayer();
                        }

                        break;

                    case E_ZombieMove.Moving:

                        if (_inRangeOfAttackable.Value == true)
                        {
                            _ZombieMove = E_ZombieMove.Idle;
                            _zombieAgent.SetDestination(transform.position);

                        }
                        else if (_isBarricadeDestroyed.Value == true)
                        {
                            TargetPlayer();
                        }
                        else
                        {
                            MoveUnit();
                        }

                        break;

                }

                #endregion


                #region [ Attacking ]

                switch (_ZombieAction)
                {

                    case E_ZombieAction.Idle:

                        if(_inRangeOfAttackable.Value == true)
                        {
                            _ZombieAction = E_ZombieAction.Attacking;
                        }

                        break;

                    case E_ZombieAction.Attacking:

                        if (_inRangeOfAttackable.Value == false)
                        {
                            _ZombieAction = E_ZombieAction.Idle;
                        }
                        else
                        {
                            AttackTarget();
                        }

                        break;

                }

                #endregion

            }));
    }


    protected IEnumerator DeadState()
    {
        EventManager.instance.SendEvent(EventIDs._EVENT_ZOMBIE_DIED);
        _zombieAgent.SetDestination(transform.position);
        yield return new WaitForSeconds(_timeTillDeath);
        Destroy(_impactEffect);
        Destroy(gameObject);
    }


    #endregion


    #region [ Interfaces ]

    public void OnHit(float value)
    {
        _unitHP -= value;
    }

    public IReactiveProperty<bool> UnitDead()
    {
        return IsUnitDead;
    }

    #endregion


}
