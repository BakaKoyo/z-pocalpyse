﻿public enum E_ZombieStatus
{
    Alive,
    Dead,
}

public enum E_ZombieMove
{
    Idle,
    Moving,
}

public enum E_ZombieAction
{
    Idle,
    Attacking,
}

public enum E_ZombieType
{
    Normal,
    Runner,
    Tanky,
}
