﻿using UnityEngine;
using UniRx;

public interface I_Attackable
{

    void OnHit(float value);

    IReactiveProperty<bool> UnitDead();

}