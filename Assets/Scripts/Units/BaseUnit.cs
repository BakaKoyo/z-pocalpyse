﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public abstract class BaseUnit : MonoBehaviour
{

    [Header("Unit Properties")]
    [SerializeField] [Range(0f, 500f)] protected float _unitHP;
    [SerializeField] [Range(0f, 20f)] protected float _unitMoveSpeed;

    [Header("Unit Components")]
    [SerializeField] protected Rigidbody _unitRB;
    [SerializeField] protected Animator _unitAnimator;

    protected ReactiveProperty<bool> _isUnitDead = new ReactiveProperty<bool>(false);

    protected IReactiveProperty<bool> IsUnitDead => _isUnitDead;

    /* Disposables for streams */
    protected List<IDisposable> _listener_UnitStatusStreams = new List<IDisposable>();

    #region [ Units ]

    protected bool CheckIfUnitIsDead(float targetHP)
    {
        if(targetHP <= 0f)
        {
            return true;
        }
        else return false;
    }

    #endregion


    #region [ Helpers ]

    /*
    Used for getting scripts
    */
    protected T GetTypeOf<T>(T param)
    {
        if (GetComponent<T>() != null)
        {
            return param = GetComponent<T>();
        }
        else return param;
    }

    #endregion


    protected void PlayAnimation(Animator animator, string paramName,
                                 float targetValue)
    {
        animator.SetFloat(paramName, targetValue);
    }


}
