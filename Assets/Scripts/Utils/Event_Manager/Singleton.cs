﻿
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T: MonoBehaviour
{

    protected static T _instance;

    public static T instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<T>();
            }
            return _instance;
        }
    }

    void Awake()
    {
        if(instance != this)
        {
            Destroy(gameObject);
        }
        OnAwake();
    }

    protected abstract void OnAwake();

}
