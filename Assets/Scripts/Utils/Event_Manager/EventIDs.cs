﻿public static class EventIDs
{

    /* [ Tags ] */
    public static readonly string _TAG_PLAYER = "_tagPlayer";
    public static readonly string _TAG_ENEMY = "_tagEnemy";
    public static readonly string _TAG_BARRIER = "_tagBarrier";

    /* [ Player UI Control Events ] */
    public static readonly string _ON_MOVE_UP = "_controlMoveUpBtn";
    public static readonly string _ON_MOVE_DOWN = "_controlMoveDownBtn";
    public static readonly string _ON_SHOOT_GUN = "_controlShootGunBtn";

    /* [ UI Events ] */
    public static readonly string _UPDATE_BARRIER_HP = "_eventUpdateBarrierHP";
    public static readonly string _UPDATE_AMMO_TEXT = "_eventUpdateAmmoText";
    public static readonly string _UPDATE_WAVE_TEXT = "_eventUpdateWaveText";

    /* [ Events ] */
    public static readonly string _EVENT_PLAYER_DEAD = "_eventPlayerDead";
    public static readonly string _EVENT_ZOMBIE_DIED = "_eventZombieDied";

    /* [ Animation Params ] */
    public static readonly string _ANIMATION_MOVEMENT_PARAM = "_movementAnimator";
    public static readonly string _ANIMATION_ACTION_PARAM = "_actionAnimator";


}
