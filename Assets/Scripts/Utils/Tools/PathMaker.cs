﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PathMaker : MonoBehaviour
{
    [Header("Path List")]
    public List<Vector3> _wayPoints = new List<Vector3>();

    [Header("Settings")]
    [Range(0f, 1f)] public float _knobSize = 0.5f;
    [Range(0f, 5f)] public float _lineSpace = 4f;
}
