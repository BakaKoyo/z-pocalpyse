﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathMaker))]
public class PathMakerEditor : Editor
{

    /* Reference of needed Script(s) */
    private PathMaker _pathMaker;
    private SelectionInfo _selectionInfo;


    /* Trigger for rePainting the scene */
    private bool _needRepaintScene;

    private void OnSceneGUI()
    {
        /* Get current Event */
        Event guiEvent = Event.current;

        if (guiEvent.type == EventType.Repaint)
        {
            DrawPoints();
        }
        else if (guiEvent.type == EventType.Layout)
        {
            /* 
            Prevents the user to having
            to click back at the gameObect
            to assign a new point to the 
            List of Vector3 from the 
            'PathMaker.cs' script
            */
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        }
        else
        {
            InputControls(guiEvent);
            /* 
            Repaint the Scene to avoid
            missing points not being 
            vissible to the user
            */
            if (_needRepaintScene == true)
            {
                HandleUtility.Repaint();
            }
        }

    }

    private void InputControls(Event guiEvent)
    {
        /* Get position of the mouse in World Space */
        Ray mouseRay = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition);

        /* 
        Choose a height for the target plane height
        and then project the ray to the plane
        */
        float planeHeight = 0;
        float distanceToPlaneHeight = (planeHeight - mouseRay.origin.y) / mouseRay.direction.y;
        Vector3 mousePosition = mouseRay.GetPoint(distanceToPlaneHeight);

        /*
        Add the coordinate based on the
        mouse position to the list of
        points in `PathMaker.cs` script
        */
        if (guiEvent.type == EventType.MouseDown &&
           guiEvent.button == 0 &&
           guiEvent.modifiers == EventModifiers.None)
        {
            OnKnobLeftMouseDown(mousePosition);
        }
        if (guiEvent.type == EventType.MouseUp &&
           guiEvent.button == 0 &&
           guiEvent.modifiers == EventModifiers.None)
        {
            OnKnobLeftMouseUp(mousePosition);
        }
        if (guiEvent.type == EventType.MouseDrag &&
           guiEvent.button == 0 &&
           guiEvent.modifiers == EventModifiers.None)
        {
            OnKnobDrag(mousePosition);
        }

        if (!_selectionInfo._isPointSelected)
        {
            MouseOverSelection(mousePosition);
        }

    }

    private void OnKnobLeftMouseDown(Vector3 mousePosition)
    {

        if (!_selectionInfo._isMouseOverKnob)
        {
            /* 
            Ability to do ctrl + z
            to undo points that the
            user has created 
            */
            Undo.RecordObject(_pathMaker, "Add point");

            /*
            Adds the point to the list 
            and sets the need to repaint 
            scene to true
            */
            _pathMaker._wayPoints.Add(mousePosition);

            _selectionInfo._pointIndex = _pathMaker._wayPoints.Count - 1;
        }

        _selectionInfo._isPointSelected = true;
        _selectionInfo._positionAtStartOfDrag = mousePosition;
        _needRepaintScene = true;
    }

    private void OnKnobLeftMouseUp(Vector3 mousePosition)
    {
        if (_selectionInfo._isPointSelected)
        {
            /* 
            Ability to undo point move 
            */
            _pathMaker._wayPoints[_selectionInfo._pointIndex] = _selectionInfo._positionAtStartOfDrag;
            Undo.RecordObject(_pathMaker, "Move Point");
            _pathMaker._wayPoints[_selectionInfo._pointIndex] = mousePosition;

            _selectionInfo._isPointSelected = false;
            _selectionInfo._pointIndex = -1;
            _needRepaintScene = true;
        }
    }

    private void OnKnobDrag(Vector3 mousePosition)
    {
        if (_selectionInfo._isPointSelected)
        {
            _pathMaker._wayPoints[_selectionInfo._pointIndex] = mousePosition;
            _needRepaintScene = true;
        }
    }

    private void MouseOverSelection(Vector3 mousePos)
    {
        /* -1 if the mouse is not in any point */
        int mouseOverPointIndex = -1;

        /* 
        Check for points that the mouse 
        could be hovering on
        */
        for (int i = 0; i < _pathMaker._wayPoints.Count; i++)
        {
            /*
            if mouse is hovering over the knob
            have the index set to that index of 
            points from the list of Vector3
            from 'PathMaker.cs' script
            */
            if (Vector3.Distance(mousePos, _pathMaker._wayPoints[i]) < _pathMaker._knobSize)
            {
                mouseOverPointIndex = i;

                /* 
                Break here because we
                already found the point
                the user mouse position
                is hovering at
                */
                break;
            }
        }

        /*
        If something has changed on 
        the mouse hover point
        */
        if (mouseOverPointIndex != _selectionInfo._pointIndex)
        {
            _selectionInfo._pointIndex = mouseOverPointIndex;
            _selectionInfo._isMouseOverKnob = mouseOverPointIndex != -1;

            _needRepaintScene = true;
        }
    }


    private void DrawPoints()
    {
        /*
        Draw the points based on the 
        quantity of the List of points
        that was added by mouseInputs
        */
        for (int i = 0; i < _pathMaker._wayPoints.Count; i++)
        {
            /* 
            In charge of drawing lines to 
            the next corresponding point
            */
            Vector3 nextPointToDraw = _pathMaker._wayPoints[(i + 1) % _pathMaker._wayPoints.Count];
            Handles.color = Color.green;
            Handles.DrawDottedLine(_pathMaker._wayPoints[i], nextPointToDraw, _pathMaker._lineSpace);

            if (i == _selectionInfo._pointIndex)
            {
                Handles.color = (_selectionInfo._isPointSelected) ? Color.red : Color.black;
            }
            else
            {
                Handles.color = Color.blue;
            }

            /* Handles.DrawSolidDisc({ Point to draw }, { Face of the Disc }, { Size of Disc }) */
            Handles.DrawSolidDisc(_pathMaker._wayPoints[i], Vector3.up, _pathMaker._knobSize);
        }
        _needRepaintScene = false;
    }


    private void OnEnable()
    {
        /* 
        Assign scripts whenever 
        the editor becomes enabled
        */
        _pathMaker = target as PathMaker;
        _selectionInfo = new SelectionInfo();
    }

    public class SelectionInfo
    {
        public int _pointIndex = -1;
        public bool _isMouseOverKnob;
        public bool _isPointSelected;
        public Vector3 _positionAtStartOfDrag;
    }

}
