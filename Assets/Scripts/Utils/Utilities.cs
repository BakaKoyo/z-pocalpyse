﻿using System;
using System.Collections;
using System.Collections.Generic;

public static class Utilities
{
   
    /*
    Cleans up the listerners
    from the passed on List of
    Disposables
    */
    public static void CleanUpListenersInStream(List<IDisposable> listenerStream)
    {
        listenerStream.ForEach(listenerInStream => listenerInStream.Dispose());
    }


    /*
    Checks the passed Variable if it's 
    0 (not set in the inspector).
        -> if it is not set 
            -> have it equal the target Value
        -> else return the value
    */
    public static float DefaultValueOf(float targetVariable, float targetValue)
    {
        if(targetVariable == 0f)
        {
            targetVariable = targetValue;
        }
        return targetVariable;
    }

}
