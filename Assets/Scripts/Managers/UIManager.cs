﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    [Header("Player HUD Info")]
    [SerializeField] private TextMeshProUGUI _barricadeText;
    [SerializeField] private TextMeshProUGUI _ammoText;
    [SerializeField] private TextMeshProUGUI _waveText;


    private void Start()
    {

        /* Player HUD Events */
        EventManager.instance.AddListener<float>(EventIDs._UPDATE_BARRIER_HP, UpdateBarrier);
        EventManager.instance.AddListener<int>(EventIDs._UPDATE_AMMO_TEXT, UpdateAmmo);
        EventManager.instance.AddListener<int>(EventIDs._UPDATE_WAVE_TEXT, UpdateWave);
    }
    private void OnDisable()
    {
        /* Player HUD Events */
        EventManager.instance.RemoveListener<float>(EventIDs._UPDATE_BARRIER_HP, UpdateBarrier);
        EventManager.instance.RemoveListener<int>(EventIDs._UPDATE_AMMO_TEXT, UpdateAmmo);
        EventManager.instance.RemoveListener<int>(EventIDs._UPDATE_WAVE_TEXT, UpdateWave);

    }


    #region [ Player HUD Events ]

    private void UpdateBarrier(float value)
    {
        ChangeUIElementOf(_barricadeText, value);
    }
    private void UpdateAmmo(int value)
    {
        ChangeUIElementOf(_ammoText, value);
    }
    private void UpdateWave(int value)
    {
        ChangeUIElementOf(_waveText, value);
    }

    private void ChangeUIElementOf(TextMeshProUGUI targetTextUI, float value)
    {
        targetTextUI.text = value.ToString();
    }
    private void ChangeUIElementOf(Text targetTextUI, int value)
    {
        targetTextUI.text = value.ToString();
    }

    #endregion


}
