﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


public class GameManager : Singleton<GameManager>
{

    [Header("Spawner Settings")]
    [SerializeField] private SpawnManager _spawner;
    [SerializeField] private GameObject _prefabToSpawn;
    [Space(5)]
    [SerializeField] [Range(0, 50)] private int _spawnIncrement;
    [SerializeField] [Range(0, 20)] private int _minSpawnAmmount;
    [SerializeField] [Range(0, 100)] private int _maxSpawnAmmount;
    [SerializeField] private int _spawnAmount;
    [Space(5)]
    [SerializeField] [Range(0f, 3f)] private float _timeToSpawn;
    [SerializeField] [Range(0f, 10f)] private float _waveTimeTillSpawn;
    [SerializeField] private int _waveCounter = 1;

    [SerializeField] private List<GameObject> _spawnedPrefabs = new List<GameObject>();
    [SerializeField] private int _currentEnemiesSpawned;
    [SerializeField] private int _currentEnemiesAlive;
    private int _minEnemiesAlive = 1;

    private ReactiveProperty<bool> _canSpawn = new ReactiveProperty<bool>(true);
    private ReactiveProperty<bool> _canSpawnEnemy = new ReactiveProperty<bool>(true);
    private ReactiveProperty<bool> _isWaveDone = new ReactiveProperty<bool>(false);
    private ReactiveProperty<bool> _isDoneCo = new ReactiveProperty<bool>(true);

    protected override void OnAwake(){ }

    private void Awake()
    {
        RandomEnemiesInWaveAmount();
    }

    private void Start()
    {
        #region [ Events ]

        EventManager.instance.AddListener(EventIDs._EVENT_PLAYER_DEAD, ChangeSpawnState);
        EventManager.instance.AddListener(EventIDs._EVENT_ZOMBIE_DIED, DeductZombies);

        #endregion


        Observable.EveryUpdate()
                  .Where(_ => _canSpawn.Value == true)
                  .Subscribe(_ =>
                  {

                      if(_canSpawnEnemy.Value == true && 
                         _isWaveDone.Value == false &&
                         _isDoneCo.Value == true &&
                         _currentEnemiesSpawned <= _spawnAmount)
                      {
                          _canSpawnEnemy.Value = false;
                          _currentEnemiesSpawned++;
                          _currentEnemiesAlive++;
                          _spawnedPrefabs.Add(_spawner.SpawnUnits(_prefabToSpawn, _spawner.transform));
                          StartCoroutine(CanSpawn());
                      }
                      else if(_isWaveDone.Value == true &&
                              _isDoneCo.Value == false)
                      {
                          StartCoroutine(WaveIncrease());
                      }
                      
                 
                  });

    }
    private void OnDisable()
    {
        EventManager.instance.RemoveListener(EventIDs._EVENT_PLAYER_DEAD, ChangeSpawnState);
    }

    #region [ Spawner ]

    private void RandomEnemiesInWaveAmount()
    {
        _spawnAmount = Random.Range(_minSpawnAmmount, _maxSpawnAmmount);
    }

    private IEnumerator CanSpawn()
    {
        _canSpawnEnemy.Value = false;
        yield return new WaitForSeconds(_timeToSpawn);
        _canSpawnEnemy.Value = true;
    }
    private IEnumerator WaveIncrease()
    {
        OnWaveEnd();
        yield return new WaitForSeconds(_waveTimeTillSpawn);
        OnWaveStart();
    }
    private void OnWaveEnd()
    {
        _isWaveDone.Value = false;
        _minSpawnAmmount += _spawnIncrement;
        _maxSpawnAmmount += _spawnIncrement;
        _waveCounter++;
        EventManager.instance.SendEvent<int>(EventIDs._UPDATE_WAVE_TEXT, _waveCounter);
        RandomEnemiesInWaveAmount();
        _currentEnemiesSpawned = 0;
        _spawnedPrefabs.Clear();
    }
    private void OnWaveStart()
    {
        _canSpawnEnemy.Value = true;
        _isDoneCo.Value = true;
    }

    /* Events */
    private void ChangeSpawnState()
    {
        _canSpawn.Value = false;
    }
    private void DeductZombies()
    {
        if(_currentEnemiesAlive <= _minEnemiesAlive)
        {
            _currentEnemiesAlive--;
            _isWaveDone.Value = true;
            _isDoneCo.Value = false;
        }
        else
        {
            _currentEnemiesAlive--;
        }
    }

    #endregion


}
