﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
   
    public GameObject SpawnUnits(GameObject PrefabToSpawn, Transform targetPositionToSpawn)
    {

        GameObject ObjToSpawn = Instantiate(PrefabToSpawn, targetPositionToSpawn);

        return ObjToSpawn;
    }

}
