﻿using UniRx;

public class ShootGunBtn : ButtonHold
{

    protected void Start()
    {
        Observable.EveryUpdate()
                  .Where(_ => _isButtonDown == true)
                  .Subscribe(_ =>
                  {
                      EventManager.instance.SendEvent(EventIDs._ON_SHOOT_GUN);
                  });
        Observable.EveryUpdate()
                  .Where(_ => _isButtonDown == false)
                  .Subscribe(_ =>
                  {
                      ResetButtonPress();
                  });
    }

}
