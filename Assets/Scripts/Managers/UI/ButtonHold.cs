﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonHold : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    protected bool _isButtonDown;

    public void OnPointerDown(PointerEventData eventData)
    {
        _isButtonDown = true;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        ResetButtonPress();
    }

    protected void ResetButtonPress()
    {
        _isButtonDown = false;
    }

}
