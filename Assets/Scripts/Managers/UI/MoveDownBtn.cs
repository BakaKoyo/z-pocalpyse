﻿using UniRx;

public class MoveDownBtn : ButtonHold
{

    private void Start()
    {
        Observable.EveryUpdate()
                  .Where(_ => _isButtonDown == true)
                  .Subscribe(_ =>
                  {
                      EventManager.instance.SendEvent(EventIDs._ON_MOVE_DOWN);
                  });
        Observable.EveryUpdate()
                  .Where(_ => _isButtonDown == false)
                  .Subscribe(_ =>
                  {
                      ResetButtonPress();
                  });
    }

}
