﻿using UniRx;

public class MoveUpBtn : ButtonHold
{

    protected void Start()
    {
        Observable.EveryUpdate()
              .Where(_ => _isButtonDown == true)
              .Subscribe(_ =>
              {
                  EventManager.instance.SendEvent(EventIDs._ON_MOVE_UP);
              });
        Observable.EveryUpdate()
                  .Where(_ => _isButtonDown == false)
                  .Subscribe(_ =>
                  {
                      ResetButtonPress();
                  });
    }

}
