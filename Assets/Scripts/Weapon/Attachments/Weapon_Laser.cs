﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Laser : MonoBehaviour
{

    private LineRenderer _laserRenderer;
    private int _startPoint = 0;
    private int _endPoint = 1;

    private void Start()
    {
        _laserRenderer = GetComponent<LineRenderer>();
    }

    public void LaserEffect(Transform laserPosition, float laserRange)
    {
        _laserRenderer.SetPosition(_startPoint, laserPosition.position);

        RaycastHit rayHit;
        if(Physics.Raycast(transform.position, transform.forward, out rayHit))
        {
            if (rayHit.collider)
            {
                _laserRenderer.SetPosition(_endPoint, rayHit.point);
            }
        }

    }

}
