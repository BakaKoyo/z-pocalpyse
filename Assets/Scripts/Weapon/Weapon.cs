﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public abstract class Weapon : MonoBehaviour
{

    [Header("Weapon Properties")]
    [SerializeField] [Range(0f, 200f)] protected float _weaponDMG;
    [SerializeField] [Range(0f, 10f)] protected float _weaponFireRate;
    [SerializeField] [Range(0f, 200f)] protected float _weaponHitForce;
    [SerializeField] [Range(0, 500)] protected int _weaponMaxAmmoCount;
    [SerializeField] [Range(0f, 10f)] protected float _weaponReloadSpeed;

    [Header("Raycast Properties")]
    [SerializeField] protected Transform _weaponShotSpot;
    [SerializeField] [Range(0f, 500f)] protected float _weaponRange;

    [Header("Weapon Effects")]
    [SerializeField] protected GameObject _weaponMuzzleFlash;
    [SerializeField] [Range(0f, 1f)] protected float _weaponShotEffectDurration;
    [SerializeField] protected GameObject _weaponImpactEffect;
    [SerializeField] [Range(0f, 1f)] protected float _weaponImpactEffectDurration;
    [SerializeField] protected GameObject _weaponBulletEffect;
    [SerializeField] [Range(0f, 100f)] protected float _weaponBulletSpeed;
    [SerializeField] [Range(0f, 1f)] protected float _weaponBulletEffectDurration;
    [SerializeField] protected Weapon_Laser _laserEffect;

    protected int _laserEndPoint = 1;

    protected ReactiveProperty<bool> _canFire = new ReactiveProperty<bool>(true);
    protected ReactiveProperty<int> _weaponCurrentAmmoCount = new ReactiveProperty<int>(0);
    protected float _weaponMinAmmoCount = 0f;


    protected void Awake()
    {
        _weaponCurrentAmmoCount.Value = _weaponMaxAmmoCount;
    }

    protected void Start()
    {

        EventManager.instance.AddListener(EventIDs._ON_SHOOT_GUN, ShootWeapon);

        Observable.EveryUpdate()
                  .Subscribe(_ =>
                  {
                     _laserEffect.LaserEffect(_weaponShotSpot, _weaponRange);
                  })
                  .AddTo(this);

        _canFire.AsObservable()
                .Where(canShoot => canShoot == false)
                .Subscribe(canShoot =>
                {
                    StartCoroutine(FireRate());
                })
                .AddTo(this);

        _weaponCurrentAmmoCount.AsObservable()
                               .Where(ammoCount => ammoCount <= _weaponMinAmmoCount)
                               .Subscribe(ammoCount =>
                               {
                                   StartCoroutine(ReloadWeapon());
                               })
                               .AddTo(this);
    }
    private void OnDisable()
    {
        EventManager.instance.RemoveListener(EventIDs._ON_SHOOT_GUN, ShootWeapon);
    }


    #region [ Functionality ]

    public void ShootWeapon()
    {

        if(_canFire.Value == true &&
           _weaponCurrentAmmoCount.Value > _weaponMinAmmoCount)
        {

            _canFire.Value = false;
            _weaponCurrentAmmoCount.Value--;
            EventManager.instance.SendEvent<int>(EventIDs._UPDATE_AMMO_TEXT, _weaponCurrentAmmoCount.Value);
            OnShootWeapon();
        }

       
    }

    protected void OnShootWeapon()
    {
        StartCoroutine(MuzzleFlash());
        StartCoroutine(BulletEffect());

        RaycastHit rayHit;

       

        if (Physics.Raycast(_weaponShotSpot.position, _weaponShotSpot.forward, out rayHit, _weaponRange))
        {
            I_Attackable unitHit = rayHit.transform.GetComponent<I_Attackable>();

            if (rayHit.transform.tag != EventIDs._TAG_PLAYER &&
                unitHit != null)
            {
                unitHit.OnHit(_weaponDMG);
            }
            if (rayHit.rigidbody != null)
            {
                rayHit.rigidbody.AddForce(-rayHit.normal * _weaponHitForce);
            }

            StartCoroutine(ImpactEffect(rayHit));

        }
    }

    protected IEnumerator FireRate()
    {
        yield return new WaitForSeconds(_weaponFireRate);
        _canFire.Value = true;
    }

    protected IEnumerator ReloadWeapon()
    {
        yield return new WaitForSeconds(_weaponReloadSpeed);
        _weaponCurrentAmmoCount.Value = _weaponMaxAmmoCount;
    }

    protected IEnumerator MuzzleFlash()
    {
        _weaponMuzzleFlash.SetActive(true);
        yield return new WaitForSeconds(_weaponShotEffectDurration);
        _weaponMuzzleFlash.SetActive(false);

    }

    protected IEnumerator BulletEffect()
    {
        GameObject bulletEffect = Instantiate(_weaponBulletEffect, _weaponMuzzleFlash.transform.position,
                                              _weaponMuzzleFlash.transform.rotation);
        bulletEffect.GetComponent<Rigidbody>().velocity = _weaponShotSpot.forward * _weaponBulletSpeed;
        yield return new WaitForSeconds(_weaponBulletEffectDurration);
        Destroy(bulletEffect);
    }

    protected IEnumerator ImpactEffect(RaycastHit hit)
    {
        GameObject impactEffect = Instantiate(_weaponImpactEffect, hit.point, Quaternion.LookRotation(hit.normal));
        yield return new WaitForSeconds(_weaponImpactEffectDurration);
        Destroy(impactEffect);
    }

    #endregion


}
